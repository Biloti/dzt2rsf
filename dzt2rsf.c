/*  dzt2rsf - convert DZT data file to RSF format
 *  Copyright (c) 2021-2022 Ricardo Biloti <biloti@unicamp.br> and
 *                          Eduardo Filpo <efilpo@gmail.com>
 *
 *  This program is free software: you can redistribute it and/or modify
 *  it under the terms of the GNU General Public License as published by
 *  the Free Software Foundation, either version 3 of the License, or
 *  (at your option) any later version.
 *
 *  This program is distributed in the hope that it will be useful,
 *  but WITHOUT ANY WARRANTY; without even the implied warranty of
 *  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *  GNU General Public License for more details.
 *
 *  You should have received a copy of the GNU General Public License
 *  along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */

#include <stdlib.h>
#include <stdio.h>
#include <string.h>

#include "dzt.h"
#include "parser.h"

#define MAXSTRLEN 1024

int main (int argc, char **argv)
{
    dztheader_t hdr;
    float * data;
    p_t *p;
    char rsffn[MAXSTRLEN];
    char hdrfn[MAXSTRLEN];
    int ntr = 0;
    double mean;
    double maxamp;
    FILE *out;

    if ((p = parse_command_line (argc, argv)) == NULL){
        return EXIT_FAILURE;
    }
    
	dzt_hdr_read(&hdr, stdin);
    
    data = (float *) malloc(sizeof(float) * hdr.rh_nsamp);

    snprintf(rsffn, MAXSTRLEN - 5, "%s.bin", p->prefix);
    snprintf(hdrfn, MAXSTRLEN - 5, "%s.rsf", p->prefix);
    
    // Binary data file
    out = fopen(rsffn, "w");
    ntr = 0;
    mean = 0;
    maxamp = 1;
    while (dzt_data_read_trace(&hdr, data, stdin)){

        for (int it = 0; it <hdr.rh_nsamp; it++){
            if (maxamp > data[it]){
                mean += data[it]/maxamp;
            }else{
                mean = 1 + mean * (maxamp/data[it]);
                maxamp = data[it];
            }
        }
        
        fwrite(data, sizeof(float), hdr.rh_nsamp, out);
        ntr++;
    }
    mean = maxamp / (ntr*hdr.rh_nsamp) * mean;

    if (p->verbose){
        printf (">>      HEADER    <<\n");
        dzt_hdr_dump(&hdr);
        printf ("\n>>   STATISTICS   <<\n");
        printf ("number of traces...: %d\n", ntr);
        printf ("amplitude max......: %f\n", maxamp);
        printf ("amplitude mean.....: %f\n", mean);
    }
    
    if (p->printmean){
        printf ("%f\n", mean);
    }
    
    fclose(out);

	// RSF header file
    out = fopen(hdrfn,"w");
    fprintf(out,"\n"
            "   n1=%i\n"
            "   n2=%i\n"
            "   o1=%f\n"
            "   o2=%f\n"
            "   d1=%e\n"
            "   d2=%e\n"
            "   data_format=\"native_float\"\n"
            "   in=%s\n",
            hdr.rh_nsamp, ntr,
            hdr.rhf_position, p->d2,
            hdr.rhf_range/(hdr.rh_nsamp-1), p->d2,
            rsffn);

    free (data);
    return 0;
}
