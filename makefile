CC=gcc

GLIB_CFLAGS = `pkg-config --cflags glib-2.0`
GLIB_LIBS   = `pkg-config --libs glib-2.0`

CFLAGS= -Wall -W -O3 $(GLIB_CFLAGS)
LIBS= $(GLIB_LIBS) -lm

OBJS= parser.o dzt.o

dzt2rsf: dzt2rsf.c $(OBJS)
	$(CC) $(CFLAGS) $^ $(LIBS) -o $@

$(OBJS): %.o: %.c %.h
	$(CC) $(CFLAGS) -c $<

install: dzt2rsf
	mkdir -p ~bin && cp $< ~/bin

clean:
	rm -f *.o *~

distclean: clean
	rm dzt2rsf

