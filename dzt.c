/*  dzt2rsf - convert DZT data file to RSF format
 *  Copyright (c) 2021-2022 Ricardo Biloti <biloti@unicamp.br> and
 *                          Eduardo Filpo <efilpo@gmail.com>
 *
 *  This program is free software: you can redistribute it and/or modify
 *  it under the terms of the GNU General Public License as published by
 *  the Free Software Foundation, either version 3 of the License, or
 *  (at your option) any later version.
 *
 *  This program is distributed in the hope that it will be useful,
 *  but WITHOUT ANY WARRANTY; without even the implied warranty of
 *  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *  GNU General Public License for more details.
 *
 *  You should have received a copy of the GNU General Public License
 *  along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */

#include <stdio.h>
#include <stdlib.h>
#include "dzt.h"

/**
	\brief Read a DZT header from file
	
	\param[out] hdr - pointer to dztheader_t
	\param[in] fp - file pointer to dzt file
	
	\return 0 on success and 1 on failure
*/
inline int dzt_hdr_read(dztheader_t *hdr, FILE *fp)
{
	return (fread(hdr, sizeof(dztheader_t), 1, fp) ? 0 : 1);

}

/**
	\brief Write a DZT header to file
	
	\param[in] hdr - pointer to dztheader_t
	\param[in] fp - file pointer to dzt file
	
	\return 0 on success and 1 on failure
*/
inline int dzt_hdr_write(dztheader_t *hdr, FILE *fp)
{
	return (fwrite(hdr, sizeof(dztheader_t), 1, fp) ? 0 : 1);

}

void dzt_hdr_dump (dztheader_t *hdr)
{
    printf("Header size........: %li\n", sizeof(dztheader_t));
    printf("rh_tag.............: %s\n", (hdr->rh_tag == 0x00ff ? "HEADER" : "OLD"));
    printf("rh_data............: %i\n", hdr->rh_data);
    printf("rh_nsamp...........: %i\n", hdr->rh_nsamp);;
    printf("rh_bits............: %i\n", hdr->rh_bits);
    printf("rh_zero............: %i\n", hdr->rh_zero);

    printf("rhf_sps............: %f\n",hdr->rhf_sps);
    printf("rhf_spm............: %f\n",hdr->rhf_spm);
    printf("rhf_mpm............: %f\n",hdr->rhf_mpm);
    printf("rhf_position.......: %.2f ns\n",hdr->rhf_position);
    printf("rhf_range..........: %.2f ns\n",hdr->rhf_range);
    
    printf("rh_npass...........: %i\n",hdr->rh_npass);

    printf("rhb_cdt............: %02i/%02i/%i %02i:%02i:%02i\n", hdr->rhb_cdt.day,hdr->rhb_cdt.month,
           hdr->rhb_cdt.year + 1980,
           hdr->rhb_cdt.hour,hdr->rhb_cdt.min,2*hdr->rhb_cdt.sec2);

    printf("rhb_mdt............: %02i/%02i/%i %02i:%02i:%02i\n", hdr->rhb_mdt.day,hdr->rhb_mdt.month,
           hdr->rhb_mdt.year + 1980,
           hdr->rhb_mdt.hour,hdr->rhb_mdt.min,2*hdr->rhb_mdt.sec2);

    printf("rh_rgain...........: %i\n",hdr->rh_rgain);
    printf("rh_nrgain..........: %i\n",hdr->rh_nrgain);
    printf("rh_text ...........: %i\n",hdr->rh_text  );
    printf("rh_ntext...........: %i\n",hdr->rh_ntext );
    printf("rh_proc ...........: %i\n",hdr->rh_proc  );
    printf("rh_nproc...........: %i\n",hdr->rh_nproc );
    printf("rh_nchan...........: %i\n",hdr->rh_nchan );

    
    printf("rhf_epsr...........: %f\n", hdr->rhf_epsr);
    printf("rhf_top............: %f\n", hdr->rhf_top);
    printf("rhf_depth..........: %f\n", hdr->rhf_depth);

    printf("rh_coordX..........: %f %f\n", hdr->rh_coordX.rh_fstart, hdr->rh_coordX.rh_fend);
    printf("rhf_servo_level....: %f\n", hdr->rhf_servo_level);
    printf("reserved...........: %s\n", hdr->reserved);
    printf("rh_accomp..........: %i\n", hdr->rh_accomp);
   
    printf("rh_sconfig.........: %i\n", hdr->rh_sconfig);
    printf("rh_spp.............: %i\n", hdr->rh_spp);
    printf("rh_linenum.........: %i\n", hdr->rh_linenum);
    
    printf("rh_coordY..........: %f %f\n", hdr->rh_coordY.rh_fstart, hdr->rh_coordY.rh_fend);

    printf("rh_lineorder.......: %i\n", hdr->rh_lineorder);
    printf("rh_slicetype.......: %i\n", hdr->rh_slicetype);
    printf("rh_dtype...........: %i\n", hdr->rh_dtype    );
    printf("rh_antname.........: %s\n", hdr->rh_antname  );
    printf("rh_pass0TX.........: %i\n", hdr->rh_pass0TX  );
    printf("rh_pass1TX.........: %i\n", hdr->rh_pass1TX  );
    printf("rh_version.........: %s\n", (hdr->rh_version == 1 ? "no GPS" : "GPS" ));
    printf("rh_system..........: %i\n", hdr->rh_system   );
    printf("rh_name............: %s\n", hdr->rh_name     );
    printf("rh_chksum..........: %i\n", hdr->rh_chksum   );

    printf("variable...........: %s\n", hdr->variable);

    printf("rh_RGPS[0].........: %4s %i %f %f %f %f\n", hdr->rh_RGPS[0].RecordType,
           hdr->rh_RGPS[0].TickCount,
           hdr->rh_RGPS[0].PositionGPS[0],
           hdr->rh_RGPS[0].PositionGPS[1],
           hdr->rh_RGPS[0].PositionGPS[2],
           hdr->rh_RGPS[0].PositionGPS[3]);
    
    printf("rh_RGPS[1].........: %4s %i %f %f %f %f\n", hdr->rh_RGPS[1].RecordType,
           hdr->rh_RGPS[1].TickCount,
           hdr->rh_RGPS[1].PositionGPS[0],
           hdr->rh_RGPS[1].PositionGPS[1],
           hdr->rh_RGPS[1].PositionGPS[2],
           hdr->rh_RGPS[1].PositionGPS[3]);
}

/**
	\brief Read one trace from a DZT data file
	
	\param[in] hdr - DZT header
	\param[out] tr - data trace (must be preallocated)
	\param[in] fp - DZT file pointer
	
	return 1 on success and 0 on failure
*/

int dzt_data_read_trace(dztheader_t *hdr, float *tr, FILE *fp)
{
	static int ns;
	static unsigned short *buffer = NULL;
	int ret;
	
	if (buffer == NULL){
		ns = hdr->rh_nsamp;
		buffer  = (unsigned short *) malloc(sizeof(unsigned short) * ns);
	}
	else if (hdr->rh_nsamp > ns){
			ns = hdr->rh_nsamp;
			buffer = realloc(buffer, sizeof(unsigned short)*ns);		
	}
	
	ret = fread(buffer, sizeof(unsigned short), ns, fp);
	if (ret != ns)
		return 0;
	
    for (int it=0; it< ns; it++)
		tr[it] = buffer[it];
	
	return 1;
}

