/*  dzt2rsf - convert DZT data file to RSF format
 *  Copyright (c) 2021-2022 Ricardo Biloti <biloti@unicamp.br> and
 *                          Eduardo Filpo <efilpo@gmail.com>
 *
 *  This program is free software: you can redistribute it and/or modify
 *  it under the terms of the GNU General Public License as published by
 *  the Free Software Foundation, either version 3 of the License, or
 *  (at your option) any later version.
 *
 *  This program is distributed in the hope that it will be useful,
 *  but WITHOUT ANY WARRANTY; without even the implied warranty of
 *  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *  GNU General Public License for more details.
 *
 *  You should have received a copy of the GNU General Public License
 *  along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */

#ifndef DZT_H
#define DZT_H

typedef struct {
    char RecordType[4];
    unsigned int TickCount;
    double PositionGPS[4];
} RGPS_t;

#define MINHEADSIZE  1024
#define PARAREASIZE   128
#define GPSAREASIZE    80  //2 * sizeof(RGPS_t);
#define INFOAREASIZE  816  //(MINHEADSIZE - PARAREASIZE - GPSAREASIZE);

typedef struct {
    unsigned sec2 :5; // seconds/2 (0-29)
    unsigned min  :6; // minute    (0-59)
    unsigned hour :5; // hour      (0-23)
    unsigned day  :5; // day       (1-31)
    unsigned month:4; // month     (1-12)
    unsigned year :7; // year      (0-127 ~ 1980-2107) 
} tagRFData_t;

typedef struct {
    float rh_fstart;
    float rh_fend;
} tagRFCoords_t;

typedef struct {
    short rh_tag;                  // 0x00ff if header - 0xfnff for old file
    short rh_data;                 // constant 1024 (obsolete)
    short rh_nsamp;                // samples per scan
    short rh_bits;                 // bits per data word (8 or 16)
    short rh_zero;                 // offset (0x80 or 0x8000 depends on rh_bits)
    float rhf_sps;                 // scans per second
    float rhf_spm;                 // scans per meter
    float rhf_mpm;                 // meters per mark
    float rhf_position;            // position (ns)
    float rhf_range;               // time range (ns)
    short rh_npass;                // num of passes for 2-D files
    tagRFData_t rhb_cdt;           // Creation date & time
    tagRFData_t rhb_mdt;           // Last modification date & time
    short rh_rgain;                // offset to range gain function
    short rh_nrgain;               // size of range gain function
    short rh_text;                 // offset to text
    short rh_ntext;                // size of text
    short rh_proc;                 // offset to processing history
    short rh_nproc;                // size of processing history
    short rh_nchan;                // number of channels
    float rhf_epsr;                // average dielectric constant
    float rhf_top;                 // position in meters
    float rhf_depth;               // range in meters
    tagRFCoords_t rh_coordX;       // X coordinates
    float rhf_servo_level;         // gain servo level
    char reserved[3];              // reserved
    unsigned char rh_accomp;       // Ant Conf component
    short rh_sconfig;              // setup config number
    short rh_spp;                  // scans per pass
    short rh_linenum;              // line number
    tagRFCoords_t rh_coordY;       // Y coordinates
    unsigned char rh_lineorder:4;  //
    unsigned char rh_slicetype:4;  //
    char rh_dtype;                 //
    char rh_antname[14];           //
    unsigned char rh_pass0TX:4;    //
    unsigned char rh_pass1TX:4;    //
    unsigned char rh_version:3;    // 1 – no GPS ou 2 - GPS
    unsigned char rh_system:5;     // 3 for SIR3000
    char rh_name[12];              // Initial File Name
    short rh_chksum;               // checksum for header
    char variable[816];            // Variable data
    RGPS_t rh_RGPS[2];             // GPS info
} __attribute__((packed)) dztheader_t;

/**
	Public functions
*/

void dzt_hdr_dump(dztheader_t *hdr);
int dzt_hdr_read (dztheader_t *hdr, FILE *fp);
int dzt_hdr_write(dztheader_t *hdr, FILE *fp);
int dzt_data_read_trace(dztheader_t *hdr, float *tr, FILE *fp);

#endif
