/*  dzt2rsf - convert DZT data file to RSF format
 *  Copyright (c) 2021-2022 Ricardo Biloti <biloti@unicamp.br> and
 *                          Eduardo Filpo <efilpo@gmail.com>
 *
 *  This program is free software: you can redistribute it and/or modify
 *  it under the terms of the GNU General Public License as published by
 *  the Free Software Foundation, either version 3 of the License, or
 *  (at your option) any later version.
 *
 *  This program is distributed in the hope that it will be useful,
 *  but WITHOUT ANY WARRANTY; without even the implied warranty of
 *  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *  GNU General Public License for more details.
 *
 *  You should have received a copy of the GNU General Public License
 *  along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */

/* Commmand-line options parser */

#include <stdio.h>
#include <glib.h>
#include "parser.h"

p_t * parse_command_line (int argc, char **argv)
{
    static p_t pp;
    p_t *p = NULL;

    static GOptionEntry entries[] = {
        {"prefix", 'p', 0, G_OPTION_ARG_FILENAME, &pp.prefix, "prefix for output RSF files", "data"},
        {"d2" , 'd', 0, G_OPTION_ARG_DOUBLE, &pp.d2, "distance between consecutive traces (in meters)", "1.0"},
        {"mean", 'm', 0, G_OPTION_ARG_NONE, &pp.printmean, "print mean value to stdout", NULL},
        {"verbose", 'v', 0, G_OPTION_ARG_NONE, &pp.verbose, "verbose output", NULL},
        {NULL}
    };

    GError *error = NULL;
    GOptionContext *parser;

    /* Set a short description for the program */
    parser = g_option_context_new ("- convert DZT to RSF format");

    /* Summary */
    g_option_context_set_summary (parser, "Convert GPR data from DZT to RSF format");

    /* Description */
    g_option_context_set_description (parser, "Copyright (c) 2021 Eduardo Filpo and Ricardo Biloti\n");

    g_option_context_add_main_entries (parser, entries, NULL);

    /* Complain about unknown options */
    g_option_context_set_ignore_unknown_options (parser, FALSE);

    /* Default values */
    pp.verbose = 0;
    pp.printmean = 0;
    pp.prefix = NULL;
    pp.d2 = 1;

    /* Parse command line */
    if (g_option_context_parse (parser, &argc, &argv, &error) == FALSE) {
        fprintf (stderr, "%s: syntax error\n", argv[0]);
        fprintf (stderr, "Try %s --help\n", argv[0]);
        return NULL;
    }

    if (pp.prefix == NULL){
        fprintf (stderr, "%s: missing prefix for output files\n", argv[0]);
        fprintf (stderr, "Try %s --help\n", argv[0]);
        return NULL;
    }

    p = (p_t*) malloc(sizeof(p_t));
    memcpy (p, &pp, sizeof(p_t));
    
    g_option_context_free (parser);

    return p;
}
