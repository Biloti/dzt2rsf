/*  dzt2rsf - convert DZT data file to RSF format
 *  Copyright (c) 2021-2022 Ricardo Biloti <biloti@unicamp.br> and
 *                          Eduardo Filpo <efilpo@gmail.com>
 *
 *  This program is free software: you can redistribute it and/or modify
 *  it under the terms of the GNU General Public License as published by
 *  the Free Software Foundation, either version 3 of the License, or
 *  (at your option) any later version.
 *
 *  This program is distributed in the hope that it will be useful,
 *  but WITHOUT ANY WARRANTY; without even the implied warranty of
 *  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *  GNU General Public License for more details.
 *
 *  You should have received a copy of the GNU General Public License
 *  along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */

#ifndef _PARSER_H
#define _PARSER_H

/* Estrutura para armazenar parâmetros e configuração. */
typedef struct {

    int verbose;
    int printmean;
    char *prefix;
    double d2;

} p_t;

p_t * parse_command_line (int argc, char **argv);

#endif
