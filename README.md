# DZT2RSF

Convert GPR raw data in DZT format to RSF

## Install

Clone this repository and install gplot with
```
git clone https://gitlab.com/Biloti/dzt2rsf.git
cd dzt2rsf
make install
```
